@extends('index')

@section('content')
	
<div class="row">
  <div class="col-sm-2 col-md-2 col-lg-2">
  </div>
  <div class="col-sm-8 col-md-8 col-lg-8 middlecol">
  	<h3 class="text-center"><b>SETTINGS</b></h3>
  	<div>
  		<ul class="nav nav-tabs settings01">
  		  <li class="active"><a href="#general">GENERAL</a></li>
  		  <li><a href="#personal">PERSONAL</a></li>
  		  <li><a href="#notification">NOTIFICATION</a></li>
  		  <li><a href="#myaccount">MY ACCOUNT</a></li>
  		  <li><a href="#verification">VERIFICATION</a></li>
  		</ul>
  		<div class="tab-content">
  		  <div id="general" class="tab-pane fade  active in">
  		  	<div class="row">
  		  		<h5><b>NAME:</b></h5>
  		  		<div class="col-sm-6">
  		  			<label>First Name*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  		<div class="col-sm-6">
  		  			<label>Last Name*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  	</div><hr>
  		  	<div class="row">
  		  		<h5><b>E-MAIL ID:</b></h5>
  		  		<div class="col-sm-6">
  		  			<input class="form-control" type="text">
  		  		</div>
  		  	</div><hr>
  		  	<div class="row">
  		  		<h5><b>ADDRESS:</b></h5>
  		  		<div class="col-sm-12">
  		  			<label>Address*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  		<div class="col-sm-6">
  		  			<label>City*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  		<div class="col-sm-6">
  		  			<label>State*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  		<div class="col-sm-6">
  		  			<label>Zip/Post Code*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  		<div class="col-sm-6">
  		  			<label>Country*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  	</div><hr>
  		  	<div class="row">
  		  		<h5><b>CONTACT NO.:</b></h5>
  		  		<div class="col-sm-6">
  		  			<label>Phone No.*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  		<div class="col-sm-6">
  		  			<label>Mobile No.*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  	</div><hr>
  		  	<div class="row">
  		  		<h5><b>LANGUANGES:</b></h5>
  		  		<div class="col-sm-6">
  		  			<label>Phone No.*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  		<div class="col-sm-6">
  		  			<label>Mobile No.*</label>
  		  			<input class="form-control" type="text">
  		  		</div>
  		  	</div><hr>
  		  	<div class="text-center">
  		  		<button type="button" class="btn ">SAVE</button>
  		  	</div><hr>

  		  </div>
  		  <div id="personal" class="tab-pane fade ">
  		  	<div class="row">
  		  		<h5><b>CHANGE PASSWORD:</b></h5>
  		  		<div class="col-sm-3">
  		  			<input class="form-control" type="password" placeholder="Old Password">
  		  		</div>
  		  		<div class="col-sm-3">
  		  			<input class="form-control" type="password" placeholder="New Password">
  		  		</div>
  		  		<div class="col-sm-3">
  		  			<input class="form-control" type="password" placeholder="Confirm Password">
  		  		</div>
  		  		<div class="col-sm-3 text-center">
  		  			<button type="button" class="btn ">SAVE</button>
  		  		</div>
  		  	</div><hr>
  		  	<div class="row">
  		  		<h5><b>SECURITY QUESTION:</b></h5>
  		  		<div class="row form-group form-group-sm">
  		  			<div class="col-sm-6">
			  		    <select class="form-control" >
			  		      <option value>Choose Question</option>
			  		      <option value="1">What is your pet's name?</option>
			  		      <option value="2">Your mother's maiden name</option>
			  		      <option value="3">Your best friend's nickname</option>
			  		      <option value="4">Your father's favorite hobby</option>
			  		      <option value="5">Your favorite city palace name</option>
			  		    </select>
			  		</div>
			  		<div class="col-sm-3">
			  			<input class="form-control" type="text" placeholder="Type Answer">	
			  		</div>
			  		<div class="col-sm-3 text-center">
			  			<button type="submit" class="btn ">SUBMIT</button>
			  		</div>
  		  		</div>
  		  	</div><hr>
  		  	<div class="row">
  		  		<h5><b>CURRENCY:</b></h5>
  		  		<div class="row form-group form-group-sm">
		  		    <div class="col-sm-6">
			  		    <select class="form-control" >
			  		      <option value>Choose Currency</option>
			  		      <option value="1">USD</option>
			  		      <option value="2">ERU</option>
			  		      <option value="3"></option>
			  		    </select>
		  		    </div>
			  	</div>
  		  	</div><hr>
  		  	<div class="row">
  		  		<h5><b>PAYMENT METHOD:</b></h5>
  		  		<div class="row form-group form-group-sm">
		  		    <div class="col-sm-6">
			  		    <select class="form-control" >
			  		      <option value>Choose Method</option>
			  		      <option value="1">Payoneer<img src="img/payoneer.png" class="imgs"></option>
			  		      <option value="2">MasterCard<img src="img/mastercard.png" class="imgs"></option>
			  		      <option value="3">Visa<img src="img/visa.png" class="imgs"></option>
			  		      <option value="3">Skrill<img src="img/skrill.png" class="imgs"></option>
			  		      <option value="3">Paypal<img src="img/paypal.png" class="imgs"></option>
			  		      <option value="3">Wester Union<img src="img/wu.png" class="imgs"></option>
			  		    </select>
		  		    </div>
		  		    <div class="col-sm-3">
			  		</div>
			  		<div class="col-sm-3 text-center">
			  			<button type="submit" class="btn ">SetUp</button>
			  		</div>
			  	</div>
  		  	</div><hr>
  		  </div>
  		  <div id="notification" class="tab-pane fade">
  		  	nnnn
  		  </div>
  		  <div id="myaccount" class="tab-pane fade">
  		  	mmmmmmmmmmmmmmhh
  		  </div>
  		  <div id="verification" class="tab-pane fade">
  		  	hhhhhhhhhhhhhhhhhhh
  		  </div>
  	</div>
  		<script>
  		$(document).ready(function(){
  		    $(".nav-tabs a").click(function(){
  		        $(this).tab('show');
  		    });
  		});
  		</script>
  </div>
  <div class="col-sm-2 col-md-2 col-lg-2">
  </div>
</div>

@stop