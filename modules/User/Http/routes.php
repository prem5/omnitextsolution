<?php

Route::group(['prefix' => 'user', 'middleware'=>'user', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
	Route::get('/', 'UserController@index');
	Route::get('/profile','UserController@profile');
	Route::get('order','UserController@order');
	Route::get('payment','UserController@payment');
	Route::get('settings','UserController@settings');
});