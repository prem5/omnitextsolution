<?php namespace Modules\User\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class UserController extends Controller {
	
	public function index()
	{
		return view('user::index');
	}
	public function profile(){
		return view('user::profile');
	}
	public function order(){
		return view('user::order');
	}
	public function payment(){
		return view('user::payment');
	}
	public function settings(){
		return view('user::settings');
	}
	
}