<?php namespace Modules\Authentication\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;

class AuthenticationController extends Controller {
	
	public function index()
	{
		return view('authentication::index');
	}
	public function register(){
		return view('authentication::register');
	}
	public function create(Request $data)
	{
		
		$validator = Validator::make($data->all(), [
			'fname' => 'required|max:255',
			'lname' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
			
		]);
		
		if ($validator->fails())
		{
			$data->flash();
			return redirect('')->with('register_validation_errors',$validator->errors()->all())->withInput();
			
		}
		$user = new User;
		$activation_code = str_random(60) . $data['email'];
		$user->lname = $data['lname'];
		$user->fname = $data['fname'];
		$user->email = $data['email'];
		$user->password = bcrypt($data['password']);
		$user->activation_code = $activation_code;
		$user->type = 1;
		if($user->save()){
			
			$to  = $data['email'];

			// subject
			$subject = 'Verification Required';

			// message
			$message = "
				<div style='line-height:1'>
					<div style='width:700px;margin:0 auto;margin-top:20px;padding:0px;background:#f5f5f5;border:1px solid #d2d2d2;border-radius:5px;font-family:Arial,Helvetica,sans-serif'>
					
					    <div style='width:660px;margin:10px 0px 0px 0px;padding:20px;font-size:12px;color:#262626;line-height:18px'>
					    	<p>Dear  <a href='mailto:{$user->email}' target='_blank'>{$user->fname } {$user->lname}</a>,</p>
					        <p>Thank you very much for signing up with Omni Text Solution. Click the button below to confirm your account.</p>

							<div style='text-align:center;padding:10px 0px'><a href='".url("authentication/activate/".$activation_code)."' style='font-size:14px;font-weight:bold;color:#fff;background:#2dcc70;text-decoration:none;border-radius:5px;padding:8px 15px' target='_blank'>Confirm</a></div>

							<p>If you think that you shouldn't have received this email, you can safely ignore it.</p>

							<p>Thank you,</p>
							<p>Omni Text Solution</p><div></div><div >
					    	
					    </div></div></div>
				</div>
			";	
			// To send HTML mail, the Content-type header must be set
			//echo $message;
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: verification@omnitext.com" . "\r\n";
			mail($to, $subject, $message, $headers);
			//return $message;
			$message = "Activation link is send to {$data['email']}.Just to be sure, please check your spam folder and activate your account" ;
			return redirect('')->with('activation_message',$message);
		}
		
	}
	public function activateAccount($code){
		if($user = User::where('activation_code',$code)->first()){
			if($user->active == 0){
				$user->active = 1;
				$user->save();
				$message = "Congratulation!! Your account is activated with email {$user->email}. Please login to your account.";
			}
			else{
				$message = " Your account is already activated with email {$user->email}. Please login to your account.";
			}
		}
		return redirect('')->with('activation_message',$message);

	}
	public function getLogin(){
		return view('authentication::login');
	}
	public function login(Request $request){
		if (Auth::attempt(['email' => $request['username'], 'password' => $request['password'],'active' => 1]))
        {
            return redirect('');
        }
        else{
        	$request->flash();
        	return redirect('')->with('login_error',1)->withInput()
					->withErrors([
						'email' => 'These credentials do not match our records.',
					])->with('login_error',1);
        }
        
	}
	
}