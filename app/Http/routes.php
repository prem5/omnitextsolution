<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('home', 'HomeController@index');
Route::get('about','WelcomeController@about');
Route::get('service','WelcomeController@service');
Route::get('whyus','WelcomeController@whyus');
Route::get('howitwork','WelcomeController@howitwork');
Route::get('faq','WelcomeController@faq');
Route::get('contact','WelcomeController@contact');
Route::get('order','WelcomeController@order');
Route::get('price','WelcomeController@price');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
