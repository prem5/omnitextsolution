@extends('index')
@section('content')
	<div class="row">
    <div class="col-sm-2 col-md-2 col-lg-2">
    </div>
    <div class="col-sm-8 col-md-8 col-lg-8 middlecol">
    <H3>Our Serives</H3>
        <p class="text-justify">With a vision to be the topmost online writing center, <a href="#">omnitextsolution.com</a> has been committed to providing a vast range of writing services. We are totally dedicated and passionate to deliver top- notch quality to our clients on their budget.</p>
        <p class="text-justify">Our services will help them ensure successful academic career to secure their bright future. As a team of proficient and professional writers, we are ever ready to assist you with these services:</p>
          <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h4>Academic Writing</h4>
                  <ul>
                    <li><a href="#">Essay Writing</a></li>
                    <li><a href="#">Research Paper</a></li>
                    <li><a href="#">Term Paper</a></li>
                    <li><a href="#">Article Critique</a></li>
                    <li><a href="#">Summary Writing</a></li>
                    <li><a href="#">Report Writing</a></li>
                    <li><a href="#">Annotated Bibliography</a></li>
                    <li><a href="#">Application Paper</a></li>
                    <li><a href="#">Annotated Bibliography</a></li>
                    <li><a href="#">Thesis/Proposal</a></li>
                    <li><a href="#">Dissertations</a></li>
                  </ul>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h4>General Writing</h4>
                  <ul>
                    <li><a href="#">Article/Blog</a></li>
                    <li><a href="#">SEO Article</a></li>
                    <li><a href="#">Copy Writing</a></li>
                    <li><a href="#">Press Releases</a></li>
                    <li><a href="#">Press Releases</a></li>
                    <li><a href="#">Newsletters</a></li>
                    <li><a href="#">Product Description</a></li>
                    <li><a href="#">E-Books</a></li>
                    <li><a href="#">Web-Contents</a></li>
                    <li><a href="#">Biography</a></li>
                  </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-6">
              <h4>Business Writing</h4>
                <ul>
                  <li><a href="#">Business Proposal</a></li>
                  <li><a href="#">Business Plan</a></li>
                  <li><a href="#">Marketing Plans</a></li>
                  <li><a href="#">Business Letters</a></li>
                  <li><a href="#">White Papers</a></li>
                  <li><a href="#">Case Study</a></li>
                  <li><a href="#">Grant Writing</a></li>
                  <li><a href="#">Presentations</a></li>
                </ul>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-6">
              <h4>Other Writing</h4>
                <ul>
                  <li><a href="#">Editing & Proofreading</a></li>
                  <li><a href="#">Resume & Cover Letters</a></li>
                  <li><a href="#">Assignments</a></li>
                </ul>
        </div>
  </div>
  <div class="col-sm-2 col-md-2 col-lg-2">
  </div>
</div>
@endsection