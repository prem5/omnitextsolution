<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Best Writing Solution</title>

    <!-- Bootstrap -->
    {!!Html::style('css/bootstrap.css')!!}
    {!!Html::style('css/bootstrap-social.css')!!}
    {!!Html::style('css/font-awesome.css')!!}
    {!!Html::style('validator/css/formValidation.min.css') !!}
    {!!Html::style('css/style.css')!!}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {!!Html::script('js/jquery.js')!!}
  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <a class="hidden-xs navbar-brand" href="#"><img src="{{url('img/omni2.png')}}"></a>
        <a class="visible-xs navbar-brand" href="#"><img src="{{url('img/omni1.png')}}"></a>
        <div class="pull-right">
          @if(Auth::check())
          <!-- <img class="navbar-brand pimg" src="img/p_img.png"> -->
        <a class="navbar-brand ashok" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><b>Hi, {{Auth::user()->fname}} {{Auth::user()->mname}} </b><span class="caret"></span></a>
          <ul class="dropdown-menu logd">
            <li><a href="{{url('user/profile')}}">My Profile <span class="glyphicon glyphicon-user pull-right" aria-hidden="true"></span></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{url('user/order')}}">My Orders <span class="glyphicon glyphicon-shopping-cart pull-right" aria-hidden="true"></span></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{url('user/payment')}}">Payments <span class="glyphicon glyphicon-briefcase pull-right" aria-hidden="true"></span></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{url('user/settings')}}">Settings <span class="glyphicon glyphicon-cog pull-right" aria-hidden="true"></span></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{url('auth/logout')}}">Log Out <span class="glyphicon glyphicon-log-out pull-right" aria-hidden="true"></span></a></li>
          </ul> 
          @else
          <span class="navbar-brand" data-toggle="modal" data-target="#login">
            <button type="button" class="btn btn-sm">My Account</button>
          </span>
          @endif
            <button type="button" class="navbar-toggle new-togle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
             
        </div>
        
      
      
        <div class="container">
        <div class="row">
        </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="text-center"> <a href="{{url('')}}">HOME</a></li>
              <li class="text-center"> <a href="{{url('service')}}">SERVICES</a></li>
              <li class="text-center"> <a href="{{url('whyus')}}">WHY US</a></li>
              <li class="text-center"> <a href="{{url('howitwork')}}">HOW IT WORKS</a></li>
              <li class="text-center"> <a href="{{url('price')}}">PRICE</a></li>
              <li class="text-center"> <a href="{{url('order')}}">ORDER</a></li>
              <li class="text-center">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MORE<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="text-center"> <a href="{{url('about')}}">ABOUT US</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="text-center"> <a href="{{url('faq')}}">FAQ</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="text-center"> <a href="{{url('contact')}}">CONTACT</a></li>
                  </ul>
              </li>
            </ul>
          </div>
      </div>

          
      </div>
    </div>

    <!-- main Body Start -->
    <div class="container">
      <div class="maincontents">
    @yield('content')

      </div>
    </div>
</div>
    <div class="footergap"></div>
    <nav class="navbar navbar-default navbar-fixed-bottom">
      <div class="container">
        <i style="line-height: 2.5" class="copyright"></ul>Copyright &copy; 2015 <a href="http://omnitextsolution.com">Omni Text Solution</a>. All Rights Reserved.</i>
        <div class="pull-right">
          
          <a href="http://facebook.com/" class="btn btn-social-icon btn-facebook" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-facebook']);"><span class="fa fa-facebook"></span></a>
          <a href="https://twitter.com/" class="btn btn-social-icon btn-twitter" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-twitter']);"><span class="fa fa-twitter"></span></a>
          <a href="http://www.gmail.com/" class="btn btn-social-icon btn-google" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-google']);"><span class="fa fa-google-plus"></span></a>
          <a href="http://www.yahoomail.com/" class="btn btn-social-icon btn-yahoo" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-yahoo']);"><span class="fa fa-yahoo"></span></a>
          <a href="#">
            <button type="button" class="btn btn-default">
              <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
            </button>
          </a>
        </div>
      </div>
    </nav>
    @if(session('login_error'))
        <script type="text/javascript">
          $(document).ready(function(){
            $("#login").modal('show');
          });
        </script>
    @endif
   
    <!-- for modal -->
    <!-- start of login modal  -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="myModalLabel">Login to your account</h4>
          </div>
          <div class="modal-body">

            
              @if (count($errors) > 0)
                <div class=" alert alert-danger">
                  <strong>Whoops!</strong> There were some problems with your input.<br><br>
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              {!!Form::open(array('id'=>'loginForm','url'=>'authentication/login','method'=>'post'))!!}
                
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" name="username"  value="{{old('username')}}" class="form-control" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control"  placeholder="Password">
              </div>
              <div class="form-group">
                              <div class="checkbox">
                                <label>
                                  <input class="pull-left" name="remember" type="checkbox" style="width: 15px;" /> Remember Me
                                </label>
                              </div>
                            </div>
              <button type="submit" class="btn btn-default">Login</button>
            {!! Form::close() !!}
            <h6><a href="">Forgot your password?</a><h6>
            <h6>New Member? <a data-toggle="modal" href="#signup">Create New Account</a><h6>
        </div>
      </div>
    </div>
      

      <!-- end of login modal -->
      <!-- start of signup form -->
      <div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="login">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="myModalLabel">Register</h4>
          </div>
          <div class="modal-body">
              <!-- <form class="form-horizontal  text-center"> -->
                {!! Form::open(array('url'=>'authentication/register','method'=>'post','class'=>'form-horizontal  text-center' ,'id'=>'signupform1')) !!}
                    <div class="form-group">
                      <div>
                        <input type="text" name="fname"  value="{{old('fname')}}" class="input-block-level rgd" required="required" placeholder="Your First Name">
                        </div>
                    </div>
                    <div class="form-group">
                      <div>
                        <input type="text" name="lname" value="{{old('lname')}}" class="input-block-level rgd" required="required" placeholder="Your Last Name">
                        </div>
                    </div>
                    <div class="form-group">
                      <div>
                        <input type="email"  name ="email" value="{{old('email')}}" class="input-block-level  rgd" required="required" placeholder="Your Email Address">
                        </div>
                    </div>
                    <div class="form-group">
                      <div>
                        <input  type="password"  id="password" name ="password" class="input-block-level rgd" required="required" placeholder="Password">
                        </div>
                      </div>
                      <div class="form-group">
                      <div>
                        <input type="password"  name ="password_confirmation" class="input-block-level rgd" required="required" placeholder="Confirm Password">
                        </div>
                      </div>
                      <!-- <div >
                        <label>
                          <input type="checkbox"> Accept terms & conditions.
                        </label>
                      </div> -->
                        <div >
                        
                           You accept our <label>  terms & conditions </label> by creating your account.
                        
                      </div>
                      <div class="form-group">
                      <div>
                        <button type="submit" class="btn  input-block-level rgd ">Create New Account</button>
                        </div>
                    </div>
                  {!! Form::close() !!}
            
      </div>
      
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

    
        
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    

 
    {!!Html::script('js/bootstrap.min.js')!!}
    {!!Html::script('validator/js/formValidation.min.js') !!}
    {!!Html::script('validator/js/formValidationBootstrap.js') !!}
    {!!Html::script('validator/js/custom.js') !!}
    <script>
$(document).ready(function() {
    $('#loginForm').formValidation({
      err: {
            container: 'tooltip'
        },
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'The email is required'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    },
                     stringLength: {
                            min: 6,
                            message: 'The password must be 6 characters long'
                        }
                }
            },
           
        }
    });

   
});

</script>


  </body>
</html>
