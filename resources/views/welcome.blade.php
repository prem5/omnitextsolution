<!-- by prem kumar singh @ 2015/10/29 -->
@extends('index')

@section('content')


	<div class="row">
		<div class="col-sm-4 col-md-4 col-lg-4 leftbanner">
			<div class=" text-center">
				<h4>Greater success</h4>
				<h4>with custom content</h4>
				<h5>Omni Text Solution authors deliver high-quality content – simply, quickly, and at competitive pricing</h5>
				<!-- <h5>Textbroker authors deliver high-quality content – simply, quickly, and at competitive pricing.</h5> -->
			</div>
				<div class="text-justify">
					<h4 class="text-center"><b>Start making money with your writing skills and creative thinking ability!</b></h4>
				  	
				  		  	{!! Form::open(array('url'=>'authentication/register','method'=>'post','class'=>'form-horizontal  text-center' ,'id'=>'signupform')) !!}
				  		  	  <div class="form-group">
				  		  	    <div>
				  		  	    	<input type="text" name="fname"  value="{{old('fname')}}" class="input-block-level rgd" required="required" placeholder="Your First Name">
				  	            </div>
				  		  	  </div>
				  		  	  <div class="form-group">
				  		  	    <div>
				  		  	    	<input type="text" name="lname" value="{{old('lname')}}" class="input-block-level rgd" required="required" placeholder="Your Last Name">
				  	            </div>
				  		  	  </div>
				  		  	  <div class="form-group">
				  		  	    <div>
				  		  	    	<input type="email"  name ="email" value="{{old('email')}}" class="input-block-level  rgd" required="required" placeholder="Your Email Address">
				  	            </div>
				  		  	  </div>
				  		  	  <div class="form-group">
				  		  	    <div>
				  		  	    	<input  type="password"  id="password" name ="password" class="input-block-level rgd" required="required" placeholder="Password">
				  	            </div>
				  	          </div>
				  	          <div class="form-group">
				  		  	    <div>
				  		  	    	<input type="password"  name ="password_confirmation" class="input-block-level rgd" required="required" placeholder="Confirm Password">
				  	            </div>
				  	          </div>
				  	          <!-- <div >
				  	            <label>
				  	              <input type="checkbox"> Accept terms & conditions.
				  	            </label>
				  	          </div> -->
				  	          <div class="form-group">
				  		  	    <div>
				  		  	    	<button type="submit" class="btn  input-block-level rgd">Create New Account</button>
				  	            </div>
				  		  	  </div>
				  		  	{!! Form::close() !!}
				</div>

			
			<!--
			<div id="carousel-example-generic" class="carousel slide" data-ridde="carousel">
				<ol class="carousel-indicators">
					<li data-taget="#carousel-example-generic" data-slide-to"0" class="active"></li>
					<li data-taget="#carousel-example-generic" data-slide-to"1"></li>
					<li data-taget="#carousel-example-generic" data-slide-to"2"></li>
					<li data-taget="#carousel-example-generic" data-slide-to"3"></li> 
				</ol> 
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="{{url('img/img3.png')}}" height="100" alt="...">
						<div class="carousel-caption">
							.....
						</div>
					</div>

					<div class="item">
						<img src="img/img2.png" alt="...">
						<div class="carousel-caption">
							.....
						</div>
					</div>

					<div class="item">
						<img src="img/img3.png" alt="...">
						<div class="carousel-caption">
							.....
						</div>
					</div>

					<div class="item">
						<img src="img/img4.png" alt="...">
						<div class="carousel-caption">
							.....
						</div>
					</div>
					.....
				</div>
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				</a>
			</div>
		-->
		</div>
		<div class="col-sm-4 col-md-4 col-lg-4 centerbanner">
			<img src="{{url('img/b_img.png')}}" class="img-responsive hidden-xs" alt="Responsive image">
		</div>
		<div class="col-sm-4 col-md-4 col-lg-4 rightbanner">
			
			
		</div>
	</div>
	<div class="bottomsection">
		<div class="row">
			<div class="col-sm-3 col-md-3 col-lg-3 text-center">
				<h4><b>ADDRESS</b></h4>
					<ul class="list-unstyled">
						<li>Kupondole, Lalitpur Nepal</li>
						<li>info@omnitextsolution.com</li>
						<li>+9779815720356</li>
					</ul>
			</div>
			<div class="col-sm-4 col-md-4 col-lg-4 text-center">
				<h4><b>OUR COMPANY</b></h4>
				<ul class="list-unstyled">
					<li>Terms & Conditions</li>
					<li>Privacy Policy</li>
					<li>Money Back Guarantee</li>
				</ul>
			</div>
			<div class="col-sm-5 col-md-5 col-lg-5">
				<h4 class="text-center"><b>WE ACCEPT</b></h4>
				<div class="row text-center">

					<a target="_blank" href="https://share.payoneer.com/nav/hw0hbrzBWaDWHiTf2BmjXcbLox6qufq3WBFl4Z6prYIjyYCR1d-dSK83iuZFhVmxjqctzq5P4Q1mDvpyD2jTXg2"><img src="img/payoneer.png" class="imgs"></a>
					<a target="_blank" href="https://share.payoneer.com/nav/hw0hbrzBWaDWHiTf2BmjXcbLox6qufq3WBFl4Z6prYIjyYCR1d-dSK83iuZFhVmxjqctzq5P4Q1mDvpyD2jTXg2"><img src="img/mastercard.png" class="imgs"></a>
					<a target="_blank" href="https://share.payoneer.com/nav/hw0hbrzBWaDWHiTf2BmjXcbLox6qufq3WBFl4Z6prYIjyYCR1d-dSK83iuZFhVmxjqctzq5P4Q1mDvpyD2jTXg2"><img src="img/visa.png" class="imgs"></a>
					<a target="_blank" href="https://account.skrill.com/signup?rid=49493629"><img src="img/skrill.png" class="imgs"></a>
					<a href="#"><img src="img/paypal.png" class="imgs"></a>
					<a href="#"><img src="img/wu.png" class="imgs"></a>

				</div>
			</div>
		</div>
	</div>
	@if (session('register_validation_errors'))
		<script type="text/javascript">
			$(document).ready(function(){
				$("#myModal").modal('show');
			});
		</script>
		<div id="myModal" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header bs-callout bs-callout-danger">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		                <div >
						  <a href="#" class="alert-link">Error!</a>
						</div>
		            </div>
		            <div class="modal-body">
						<div class=" alert alert-danger">
						  <strong>Whoops!</strong> There were some problems with your input.<br><br>
						  <ul>
						    @foreach (session('register_validation_errors') as $error)
						      <li>{{ $error }}</li>
						    @endforeach
						  </ul>
						</div>
		            </div>
		        </div>
		    </div>
		</div>
	          
	        @endif
	@if(session('activation_message'))

		<script type="text/javascript">
			$(document).ready(function(){
				$("#myModal").modal('show');
			});
		</script>

		<div id="myModal" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		                <div >
						  <a href="#" class="alert-link">Success</a>
						</div>
		            </div>
		            <div class="modal-body">
						<p class="alert alert-success">{{ session('activation_message') }}</p>
		            </div>
		        </div>
		    </div>
		</div>
	@endif
@endsection




